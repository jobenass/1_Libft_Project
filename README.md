# Projet LIBFT

L'objectif est de recoder une partie de certaines librairies du langage C tel que string.h, ctype.h, stdlib.h.

Auxquelles s'ajoute aussi d'autres fonctions de manipulation de string et de structures qui sont propres au sujet.

Le but final étant d'avoir un librairie personnel évolutif utilisable sur les projets futurs.
Les restrictions des sujets amènent à utiliser cette librairie et interdit les fonctions du systems.

Notions C:
  - Utiliser le type size_t
  - Utiliser les casts
  - Initier au fonctionnement de la heap
  - Initier a la gestion d'erreurs
  - Initier aux structures
  - Générer une librairie statique
  - Composer un Makefile

Compétence:
  - Algorithme
