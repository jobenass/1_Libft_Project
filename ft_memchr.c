/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_memchr.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jobenass <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/09 16:28:27 by jobenass     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/21 17:06:54 by jobenass    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char	*p_s;
	size_t			i;

	p_s = 0;
	i = 0;
	if (n >= 1)
	{
		p_s = (unsigned char *)s;
		while (i < n && p_s[i] != (unsigned char)c)
			i++;
		if (i >= n)
			return (NULL);
		p_s = (void *)&s[i];
	}
	return (p_s);
}
