# **************************************************************************** #
#                                                           LE - /             #
#                                                               /              #
#    Makefile                                         .::    .:/ .      .::    #
#                                                  +:+:+   +:    +:  +:+:+     #
#    By: jobenass <marvin@le-101.fr>                +:+   +:    +:    +:+      #
#                                                  #+#   #+    #+    #+#       #
#    Created: 2019/10/09 09:48:56 by jobenass     #+#   ##    ##    #+#        #
#    Updated: 2019/10/29 17:10:12 by jobenass    ###    #+. /#+    ###.fr      #
#                                                          /                   #
#                                                         /                    #
# **************************************************************************** #

NAME	= libft.a

CC		= gcc

CFLAGS	= -Wall -Wextra -Werror

HEADS	= libft.h

LFLAGS	= -I ${HEADS}

SRCS	= ft_memset.c
SRCS	+= ft_bzero.c
SRCS	+= ft_memcpy.c
SRCS	+= ft_memccpy.c
SRCS	+= ft_memmove.c
SRCS	+= ft_memchr.c
SRCS	+= ft_memcmp.c
SRCS	+= ft_strlen.c
SRCS	+= ft_isalpha.c
SRCS	+= ft_isdigit.c
SRCS	+= ft_isalnum.c
SRCS	+= ft_isascii.c
SRCS	+= ft_isprint.c
SRCS	+= ft_toupper.c
SRCS	+= ft_tolower.c
SRCS	+= ft_strchr.c
SRCS	+= ft_strrchr.c
SRCS	+= ft_strncmp.c
SRCS	+= ft_strlcpy.c
SRCS	+= ft_strlcat.c
SRCS	+= ft_strnstr.c
SRCS	+= ft_atoi.c
SRCS	+= ft_calloc.c
SRCS	+= ft_strdup.c
SRCS	+= ft_substr.c
SRCS	+= ft_strjoin.c
SRCS	+= ft_strtrim.c
SRCS	+= ft_split.c
SRCS	+= ft_itoa.c
SRCS	+= ft_strmapi.c
SRCS	+= ft_putchar_fd.c
SRCS	+= ft_putstr_fd.c
SRCS	+= ft_putendl_fd.c
SRCS	+= ft_putnbr_fd.c

BONUS	= ft_lstnew_bonus.c
BONUS	+= ft_lstsize_bonus.c
BONUS	+= ft_lstadd_front_bonus.c
BONUS	+= ft_lstlast_bonus.c
BONUS	+= ft_lstadd_back_bonus.c
BONUS	+= ft_lstdelone_bonus.c
BONUS	+= ft_lstclear_bonus.c
BONUS	+= ft_lstiter_bonus.c
BONUS	+= ft_lstmap_bonus.c

OBJS	= ${SRCS:.c=.o}

BONS	= ${BONUS:.c=.o}

all:	${NAME}

$(NAME):	${OBJS} ${HEADS}
	ar rc ${NAME} ${OBJS}

bonus:		${BONS} ${OBJS} ${HEADS}
	ar rc ${NAME} ${BONS} ${OBJS}

clean:
	rm -f ${OBJS} ${BONS}

fclean:		clean
	rm -f ${NAME}

re:		fclean all

.PHONY:		all clean fclean re
