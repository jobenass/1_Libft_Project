/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_lstclear_bonus.c                              .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jobenass <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/23 13:35:51 by jobenass     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/23 16:15:22 by jobenass    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstclear(t_list **lst, void (*del)(void *))
{
	t_list		*tmp;

	if (!(*lst))
		return ;
	tmp = *lst;
	while (tmp)
	{
		(*del)(tmp->content);
		free(tmp);
		tmp = tmp->next;
	}
	*lst = NULL;
}
