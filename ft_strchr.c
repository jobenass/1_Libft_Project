/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strchr.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jobenass <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/07 17:49:23 by jobenass     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/21 17:09:51 by jobenass    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *s, int c)
{
	char	*p_s;
	int		i;

	p_s = (char *)s;
	i = 0;
	while (p_s[i] && p_s[i] != (char)c)
		i++;
	if (p_s[i] == (char)c)
		return (&p_s[i]);
	return (NULL);
}
