/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_substr.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jobenass <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/11 16:59:44 by jobenass     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/22 15:49:51 by jobenass    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	char	*p_s;
	char	*copy;
	size_t	size;
	size_t	i;

	i = 0;
	p_s = 0;
	size = ft_strlen((char *)s);
	if (start < size)
		p_s = (char *)&s[(size_t)start];
	else
		p_s = (char *)&s[ft_strlen(s)];
	size = 0;
	while (p_s[size] && size < len)
		size++;
	if (!(copy = (char *)ft_calloc(size + 1, sizeof(char))))
		return (NULL);
	while (i < size && p_s[i])
	{
		copy[i] = p_s[i];
		i++;
	}
	copy[i] = '\0';
	return (copy);
}
