/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_lstmap_bonus.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jobenass <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/23 14:41:37 by jobenass     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/25 13:22:34 by jobenass    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, void *(*f)(void *), void (*del)(void *))
{
	t_list	*new;
	t_list	*start;

	if (!lst)
		return (NULL);
	if (!(new = ft_lstnew(lst->content)))
		return (NULL);
	start = new;
	new->content = (*f)(lst->content);
	lst = lst->next;
	while (lst)
	{
		if (!(new->next = ft_lstnew(lst->content)))
		{
			ft_lstclear(&start, (*del));
			return (NULL);
		}
		new = new->next;
		new->content = (*f)(lst->content);
		lst = lst->next;
	}
	return (start);
}
