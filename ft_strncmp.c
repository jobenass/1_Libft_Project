/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strncmp.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jobenass <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/08 10:33:25 by jobenass     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/21 17:11:08 by jobenass    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

int		ft_strncmp(const char *s1, const char *s2, size_t n)
{
	unsigned char	*p_s1;
	unsigned char	*p_s2;
	size_t			i;

	p_s1 = 0;
	p_s2 = 0;
	i = 0;
	if (n >= 1)
	{
		p_s1 = (unsigned char *)s1;
		p_s2 = (unsigned char *)s2;
		while (i < n - 1 && (p_s1[i] == p_s2[i]) && (p_s1[i] && p_s2[i]))
			i++;
		if (p_s1[i] != p_s2[i])
			return (p_s1[i] - p_s2[i]);
	}
	return (0);
}
